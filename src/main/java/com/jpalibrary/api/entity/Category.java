package com.jpalibrary.api.entity;


import java.util.List;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Category {
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;
    
    // Relation One to Many avec Book
    @OneToMany(mappedBy = "category")
    // Pour éviter une boucle infinie (récursivité)
    @JsonIgnoreProperties("category")
    private List<Book> books;

    public Category() {}

    // Méthode pour obtenir l'identifiant de la catégorie
    public Long getId() {
        return this.id;
    }
    // Méthode pour définir l'identifiant de la catégorie
    public void setId(Long id) {
        this.id = id;
    }
    // Méthode pour obtenir le nom de la catégorie
    public String getName() {
        return this.name;
    }
    // Méthode pour définir le nom de la catégorie
    public void setName(String name) {
        this.name = name;
    }
    // Méthode pour obtenir la liste des livres de la catégorie
    /*public List<Book> getBooks() {
        return this.books;
    }
    // Méthode pour définir la liste des livres de la catégorie
    public void setBooks(List<Book> books) {
        this.books = books;
    }*/

}
