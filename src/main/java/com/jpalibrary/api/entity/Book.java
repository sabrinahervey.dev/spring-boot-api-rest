package com.jpalibrary.api.entity;

import java.util.Set;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;

// @entity signifie que cette class book est une entity persistence, elle représente une table dans la base de données.
@Entity
public class Book {

    @Id //spécifie que "id" est la clé primaire de cette classe entity
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long bookid;

    @Column (length = 100, nullable = false)
    private String title;

    @Column(columnDefinition = "TEXT" )
    private String description;

    @Column(columnDefinition = "BOOLEAN DEFAULT true")
    private boolean available = true;
    
    // Relation Many To One avec Category
    @ManyToOne
    //@JoinColumn(name ="category_id")
    //@JsonIgnoreProperties("books")
    private Category category;

    // Relation Many to Many avec Author
    @ManyToMany
    // Pour éviter une boucle infinie (récursivité).
    @JsonIgnoreProperties("books")
    private Set<Author> authors;


    public Book() {}
    
    // Méthode pour obtenir l'identifiant du livre
    public Long getBookid() {
        return this.bookid;
    }
    // Méthode pour définir l'identifiant du livre
    public void setBookid(Long bookid) {
        this.bookid = bookid;
    }
    // Méthode pour obtenir le titre du livre
    public String getTitle() {
        return this.title;
    }
    // Méthode pour définir le titre du livre
    public void setTitle(String booktitle) {
        this.title = booktitle;
    }
    // Méthode pour obtenir la description du livre
    public String getDescription() {
        return this.description;
    }
    // Méthode pour définir la description du livre
    public void setDescription(String description) {
        this.description = description;
    }
    // Méthode pour définir la disponibilité du livre
    public boolean isAvailable() {
        return this.available;
    }

    public boolean getAvailable() {
        return this.available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
    // Méthode pour obtenir la catégorie du livre
    public Category getCategory() {
        return this.category;
    }
    // Méthode pour définir la catégorie du livre
    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Author> getAuthors() {
        return this.authors;
    }

    /*public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }*/

}
   

   