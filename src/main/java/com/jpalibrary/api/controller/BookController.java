package com.jpalibrary.api.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpalibrary.api.entity.Book;
import com.jpalibrary.api.service.BookService;

import jakarta.persistence.EntityNotFoundException;



// le fichier controller contient les endpoints(routes)
//envoie et reçois les réponses (requetes)

@RestController
@RequestMapping("/books")
public class BookController { 

      private BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    //route (endpoint) pour afficher les livres 
    @GetMapping("/")
    
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }
     // route(endpoint) pour obtenir un livre par son identifiant
    @GetMapping("/{id}")
    public ResponseEntity<Book> getBookById(@PathVariable Long id) {
        //return bookService.getBookById(id);
        try {
            // renvoie le livre et le statut de la requête (OK si trouvé, NOT_FOUND sinon)
            return new ResponseEntity<Book>(bookService.getBookById(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }

    }
    // Endpoint pour créer un nouveau livre (POST)
    @PostMapping("/")
    public Book postBook(@RequestBody Book book) {
        return this.bookService.createOrUpdateBook(book);
    }
    // Endpoint pour mettre à jour un livre existant (PUT)
    @PutMapping("/{id}")
    public Book updateBook(@PathVariable Long id, @RequestBody Book book) {
        book.setBookid(id);
        return bookService.createOrUpdateBook(book);
    }
    // Endpoint pour supprimer un livre par son identifiant (DELETE)
    @DeleteMapping("/{id}")
    public void deleteBook(@PathVariable Long id) {
        bookService.deleteBook(id);
     
    }
    //endpoint pour trouver un livre par son titre
    @GetMapping("/title/{title}")
    public Book getBookByTitle(@PathVariable String title) {
        return bookService.getBookByTitle(title);
    }
}

 

  