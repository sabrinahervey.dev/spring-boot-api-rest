package com.jpalibrary.api.controller;

import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
//import java.util.HashSet;
import com.jpalibrary.api.entity.Author;
import com.jpalibrary.api.service.AuthorService;

@RestController
@RequestMapping("/authors")
public class AuthorController {

    public AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }
    @GetMapping("/")
    public Set<Author> getAllAuthors() {
        
        return authorService.getAllAuthors();
    }

    // Endpoint pour créer un author (POST)
    @PostMapping("/")
    public Author postAuthor(@RequestBody Author author) {
        return this.authorService.createOrUpdateAuthor(author);
    }

    @PutMapping("/{id}")
    public Author updateAuthor(@PathVariable("id") Long id, @RequestBody Author author) {
        return authorService.updateAuthor(id, author);
    }


}
