package com.jpalibrary.api.service;

import java.util.List;


import org.springframework.stereotype.Service;

import com.jpalibrary.api.entity.Category;
import com.jpalibrary.api.repository.CategoryRepository;

@Service
public class CategoryService {
   
    private CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepoInjected) {
        this.categoryRepository = categoryRepoInjected;
    }

    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    public Category createOrUpdateCategory(Category category) {
        return categoryRepository.save(category);
    }

    public void deleteCategory(Long id) {
        categoryRepository.deleteById(id);
    }

}
