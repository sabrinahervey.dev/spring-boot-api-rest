// fait des traitements (vérifier les données, charger des infos de la base de données)
package com.jpalibrary.api.service;

import java.util.List;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpalibrary.api.entity.Book;
import com.jpalibrary.api.repository.BookRepository;




@Service
public class BookService {
    
    private BookRepository bookRepository;
    
    public BookService(BookRepository bookServiceInjected) {
        this.bookRepository = bookServiceInjected;
    }
    
    public List<Book> getAllBooks() {
        return this.bookRepository.findAll();
    }
    
    public Book getBookById(Long id) {
        return bookRepository.findById(id).orElse(null);
    }

    public Book createOrUpdateBook(Book book) {
        return bookRepository.save(book);
    }

    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    public Book getBookByTitle(String title) {
        return bookRepository.findBytitle(title);
    }
    
} 



