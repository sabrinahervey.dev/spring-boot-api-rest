package com.jpalibrary.api.service;

import java.util.List;
import java.util.Set;
import java.util.HashSet;
import org.springframework.stereotype.Service;


import com.jpalibrary.api.entity.Author;
import com.jpalibrary.api.repository.AuthorRepository;


@Service
public class AuthorService {
    
     private AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepoInjected) {
        this.authorRepository = authorRepoInjected;
    }
   
    public Set<Author> getAllAuthors() {
        List<Author> authors = authorRepository.findAll();
        return new HashSet<>(authors);
    }

    public Author createOrUpdateAuthor(Author author) {
        return authorRepository.save(author);
    }

    public void deleteAuthor(Long id) {
        authorRepository.deleteById(id);
    }
    public Author updateAuthor(Long id, Author author) {
        // cherche par id et récupère l'auteur
        Author currentAuthor = authorRepository.findById(id).get();
        // remplace les données par les nouvelles
        currentAuthor.setFirstName(author.getFirstName());
        currentAuthor.setLastName(author.getLastName());

        // sauvegarde et retourne l'auteur mis à jour
        return authorRepository.save(currentAuthor);

    }

}
