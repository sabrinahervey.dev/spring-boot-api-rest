package com.jpalibrary.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jpalibrary.api.entity.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
    
}
