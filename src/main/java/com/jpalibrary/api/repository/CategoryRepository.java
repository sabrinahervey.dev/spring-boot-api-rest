package com.jpalibrary.api.repository;

//import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jpalibrary.api.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {


}