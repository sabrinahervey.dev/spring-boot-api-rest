package com.jpalibrary.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.jpalibrary.api.entity.Book;

//permet la connection entre le crud et l'entité
//interagi avec la database
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    //méthode de requête automatique dérivée de Spring Data JPA.
    public Book findBytitle(String title);
 //les actions du crud sont herité de jparepository

    
}
